// Copyright (c) 2016-present Mattermost, Inc. All Rights Reserved.
// See License.txt for license information.

package model

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"strings"
)

type TeamMember struct {
	TeamId        string `json:"team_id"`
	UserId        string `json:"user_id"`
	Roles         string `json:"roles"`
	DeleteAt      int64  `json:"delete_at"`
	SchemeUser    bool   `json:"scheme_user"`
	SchemeAdmin   bool   `json:"scheme_admin"`
	ExplicitRoles string `json:"explicit_roles"`
	// Betterdot custom [begin]
	TeamName           string `json:"team_name,omitempty"`
	UserEmail          string `json:"user_email,omitempty"`
	allowNumericUserID bool   `json:"-"`
	// Betterdot custom [end]
}

type TeamUnread struct {
	TeamId       string `json:"team_id"`
	MsgCount     int64  `json:"msg_count"`
	MentionCount int64  `json:"mention_count"`
}

type TeamMemberForExport struct {
	TeamMember
	TeamName string
}

func (o *TeamMember) ToJson() string {
	b, _ := json.Marshal(o)
	return string(b)
}

func (o *TeamUnread) ToJson() string {
	b, _ := json.Marshal(o)
	return string(b)
}

func TeamMemberFromJson(data io.Reader) *TeamMember {
	var o *TeamMember
	json.NewDecoder(data).Decode(&o)
	return o
}

func TeamUnreadFromJson(data io.Reader) *TeamUnread {
	var o *TeamUnread
	json.NewDecoder(data).Decode(&o)
	return o
}

func TeamMembersToJson(o []*TeamMember) string {
	if b, err := json.Marshal(o); err != nil {
		return "[]"
	} else {
		return string(b)
	}
}

func TeamMembersFromJson(data io.Reader) []*TeamMember {
	var o []*TeamMember
	json.NewDecoder(data).Decode(&o)
	return o
}

func TeamsUnreadToJson(o []*TeamUnread) string {
	if b, err := json.Marshal(o); err != nil {
		return "[]"
	} else {
		return string(b)
	}
}

func TeamsUnreadFromJson(data io.Reader) []*TeamUnread {
	var o []*TeamUnread
	json.NewDecoder(data).Decode(&o)
	return o
}

// Betterdot custom [begin]

// SetAllowNumericUserID sets the private allowNumericUserID field, to
// indicate that numeric-only user IDs can be considered as valid.
func (o *TeamMember) SetAllowNumericUserID(allow bool) {
	o.allowNumericUserID = allow
}

// IsValid returns true if the format of the team id and user id are valid.
// In case the allowNumericUserID field is set to true,
// then an exception is made and numbers are allowed for the user id.
func (o *TeamMember) IsValid() *AppError {

	if len(o.TeamId) != 26 {
		return NewAppError("TeamMember.IsValid", "model.team_member.is_valid.team_id.app_error", nil, "", http.StatusBadRequest)
	}

	if len(o.UserId) != 26 {
		// Allow numeric IDs as an exception if size is not 26 chars
		_, err := strconv.ParseInt(o.UserId, 10, 64)
		if err != nil {
			return NewAppError("TeamMember.IsValid", "model.team_member.is_valid.user_id.app_error", nil, "", http.StatusBadRequest)
		}
	}

	return nil
}

// Betterdot custom [end]

func (o *TeamMember) PreUpdate() {
}

func (o *TeamMember) GetRoles() []string {
	return strings.Fields(o.Roles)
}
