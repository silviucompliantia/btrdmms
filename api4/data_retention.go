// Copyright (c) 2017-present Mattermost, Inc. All Rights Reserved.
// See License.txt for license information.

package api4

import (
	"encoding/json"
	"net/http"

	"github.com/mattermost/mattermost-server/model"
)

func (api *API) InitDataRetention() {
	api.BaseRoutes.DataRetention.Handle("/policy", api.ApiSessionRequired(getPolicy)).Methods("GET")
	// Betterdot custom [begin]
	api.BaseRoutes.DataRetention.Handle("/purge", api.ApiSessionRequired(purgeData)).Methods("POST")
	// Betterdot custom [end]
}

func getPolicy(c *Context, w http.ResponseWriter, r *http.Request) {
	// No permission check required.

	policy, err := c.App.GetDataRetentionPolicy()
	if err != nil {
		c.Err = err
		return
	}

	w.Write([]byte(policy.ToJson()))
}

// Betterdot custom [begin]

// purgeData removes old messages and attachments. Only sysadmins can purge data.
func purgeData(c *Context, w http.ResponseWriter, r *http.Request) {

	// Return with error if not sysadmin
	if !c.IsSystemAdmin() {
		c.SetPermissionError(model.PERMISSION_EDIT_OTHER_USERS)
		c.Err.DetailedError += ", only system administrator can purge data"
		return
	}

	props := model.MapFromJson(r.Body)

	type purgeStatsStruct struct {
		FilesDeletedCount int      `json:"files_deleted_count"`
		PostsDeletedCount int      `json:"posts_deleted_count"`
		FileErrors        []string `json:"file_errors"`
		PostErrors        []string `json:"post_errors"`
	}

	var purgeStats purgeStatsStruct
	purgeStats.FileErrors = make([]string, 0)
	purgeStats.PostErrors = make([]string, 0)

	deleteFilesBeforeDateStr := props["delete_files_before"]
	if len(deleteFilesBeforeDateStr) != 0 {
		// Parse date in yyyy-mm-dd format
		if !(model.IsValidDate(deleteFilesBeforeDateStr, "2006-01-02") ||
			model.IsValidDate(deleteFilesBeforeDateStr, "2006-01-02 15:04:05") ||
			model.IsValidDate(deleteFilesBeforeDateStr, "2006-01-02 15:04")) {
			c.SetInvalidParam("delete_files_before")
			return
		}
		// c.Log.Debug("purgeData BEFORE c.App.GetFileInfosForPost =  " + deleteFilesBeforeDateStr)
		filepaths, err := c.App.GetFileInfosForPost("path,thumbnailpath,previewpath;" + deleteFilesBeforeDateStr)
		if err != nil {
			c.Err = err
			c.Err.DetailedError += " - GetFileInfosForPost(path,thumbnailpath,previewpath;" + deleteFilesBeforeDateStr + ") mode"
			return
		}
		purgeStats.FilesDeletedCount = len(filepaths)
		if len(filepaths) > 0 {
			for _, fi := range filepaths {
				// c.Log.Debug(fmt.Sprintf("purgeData AFTER c.App.GetFileInfosForPost => %s / %s / %s", fi.Path, fi.PreviewPath, fi.ThumbnailPath))
				exists, err := c.App.FileExists(fi.Path)
				if exists && err == nil {
					// Remove the file, the thumbnail, and the preview
					if err1 := c.App.RemoveFile(fi.Path); err1 != nil {
						purgeStats.FileErrors = append(purgeStats.FileErrors, "error removing "+fi.Path+": "+err1.Error())
					}
					if fi.HasPreviewImage {
						if err2 := c.App.RemoveFile(fi.PreviewPath); err2 != nil {
							purgeStats.FileErrors = append(purgeStats.FileErrors, "error removing preview "+fi.PreviewPath+": "+err2.Error())
						}
					}
					if err3 := c.App.RemoveFile(fi.ThumbnailPath); err3 != nil {
						purgeStats.FileErrors = append(purgeStats.FileErrors, "error removing thumbnail "+fi.ThumbnailPath+": "+err3.Error())
					}
				} else if err != nil {
					purgeStats.FileErrors = append(purgeStats.FileErrors, "error for FileExists "+fi.Path+": "+err.Error())
				}
				// Remove the fileinfo record
				fiErr := c.App.PermanentlyDeleteFileInfo(fi.Id, c.App.Session.UserId)
				if fiErr != nil {
					purgeStats.FileErrors = append(purgeStats.FileErrors, "error removing fileinfo record "+fi.Id+": "+fiErr.Error())
				}
			}
		}

	}

	deletePostsBeforeDateStr := props["delete_posts_before"]
	c.Log.Debug("purgeData deletePostsBeforeDateStr=  " + deletePostsBeforeDateStr)
	if len(deletePostsBeforeDateStr) != 0 {
		// Parse date in yyyy-mm-dd format
		if !(model.IsValidDate(deletePostsBeforeDateStr, "2006-01-02") ||
			model.IsValidDate(deleteFilesBeforeDateStr, "2006-01-02 15:04:05") ||
			model.IsValidDate(deleteFilesBeforeDateStr, "2006-01-02 15:04")) {
			c.SetInvalidParam("delete_posts_before")
			return
		}
		c.Log.Debug("purgeData BEFORE c.App.GetFileInfosForPost =  " + deletePostsBeforeDateStr)
		post, err := c.App.DeletePost("rangePermanentDeletePosts;"+deletePostsBeforeDateStr, c.App.Session.UserId)
		if err != nil {
			c.Err = err
			c.Err.DetailedError += " - GetFileInfosForPost(rangePermanentDeletePosts;" + deletePostsBeforeDateStr + ") mode"
			return
		}
		postCount, ok := post.Props["count"]
		if ok {
			purgeStats.PostsDeletedCount = int(postCount.(int64))
		}
		c.App.InvalidateAllCaches()
	}

	var toJSON = func(pstats purgeStatsStruct) string {
		b, _ := json.Marshal(pstats)
		return string(b)
	}

	w.Write([]byte(toJSON(purgeStats)))

}

// Betterdot custom [end]
